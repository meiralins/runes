from digrams import digram_count


def test_one_digram():
    assert digram_count('ae') == {'ae': 1}


def test_word():
    assert digram_count('abacate') == {'ab': 1, 'ba': 1, 'ac': 1, 'ca': 1,
                                       'at': 1, 'te': 1}


def test_repeated_digrams():
    assert digram_count('arara') == {'ar': 2, 'ra': 2}


def test_two_words():
    assert digram_count('ae br') == {'ae': 1, 'br': 1}


def test_ignore_excessive_spaces():
    assert digram_count(' ab     cd   ') == {'ab': 1, 'cd': 1}


def test_ignore_accents():
    assert digram_count('áêã') == {'ae': 1, 'ea': 1}


def test_ponctuation_as_space():
    assert digram_count(' ab,cd ') == digram_count(' ab   cd  ')


def test_ignore_uppercase():
    assert digram_count('AbcD') == digram_count('abcd')
