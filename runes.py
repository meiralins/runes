from math import log
import pickle
import random
import string

from digrams import digram_count, treat


class Runes:
    def __init__(self, text='data/runes_portuguese.txt',
                 digrams='data/domcasmurro.txt.digs'):
        with open(text, 'r') as f:
            self.text = treat(f.read())
        with open(digrams, 'rb') as f:
            self.digrams = pickle.load(f)

    def plausibility(self, text):
        text_digrams = digram_count(text)
        return sum(text_digrams[digram] * log(val)
                   for digram, val in self.digrams.items())

    def _scramble(self):
        a = string.ascii_lowercase
        b = ''.join(random.sample(a, len(a)))
        trans = str.maketrans(a, b)
        self.text = self.text.translate(trans)

    def search(self, n=10000):
        self._scramble()
        self.plaus = self.plausibility(self.text)
        for _ in range(n):
            a, b = random.sample(string.ascii_lowercase, 2)
            new_text = self.swap(a, b)
            new_plausibility = self.plausibility(new_text)
            if new_plausibility > self.plaus:
                self.plaus = new_plausibility
                self.text = new_text
        return self.plaus, self.text

    def translate(self, n=10):
        return sorted((self.search() for _ in range(n)), reverse=True)

    def swap(self, a, b):
        return self.text.translate({ord(a): ord(b), ord(b): ord(a)})
