import re
import unicodedata
from collections import Counter


def pairwise(text):
    a = iter(text)
    b = iter(text)
    next(b, None)
    return (''.join(z) for z in zip(a, b) if ' ' not in z)


def treat(text):
    text = ''.join(c for c in unicodedata.normalize('NFKD', text.lower()) if
                   not unicodedata.combining(c))
    text = re.sub('[^a-zA-Z]', ' ', text).strip()
    return re.sub('\s+', ' ', text)


def digram_count(text):
    treated_text = treat(text)
    return Counter(pairwise(treated_text))
