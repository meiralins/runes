import pickle

from digrams import digram_count


def save_digrams(filename):
    with open(filename) as f:
        digrams = digram_count(f.read())
    with open(filename + '.digs', 'wb') as f:
        pickle.dump(digrams, f)


if __name__ == '__main__':
    save_digrams('data/brazcubas.txt')
    save_digrams('data/domcasmurro.txt')
